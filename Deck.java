import java.util.Random;
public class Deck{
	private Card[] cards;
	private int numberOfCards;
	private Random rng;
	
	public int getNumberOfCards(){
		return this.numberOfCards;
	}
	public Card[] getCards(){
		return this.cards;
	}

	
	public Deck(){
		this.numberOfCards=52;
		this.cards= new Card[numberOfCards];
		this.rng= new Random();
		
		String[] allSuits= {"Hearts","Diamonds","Spades","Clubs"};
		String[] allValues={"Ace","Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Jack", "Queen","King"};
		int indexOfvalue=0;
		for(int j=0;j<allSuits.length;j++){
			
			for(int i=0;i<allValues.length;i++){
				cards[indexOfvalue]= new Card(allSuits[j],allValues[i]);
				indexOfvalue++;
			}
		}
	}
	public int length(){
		return numberOfCards;
	}
	public Card drawTopCard(){
		numberOfCards--;
		return cards[numberOfCards];
	}
	public String toString(){
		String deck="";
		for(int i=0;i<numberOfCards;i++){
			deck+= cards[i] +"\n";
		}
		return deck;
	}
	public void shuffle(){
		
		for(int i=0;i<numberOfCards-1;i++){
			int randomNum = rng.nextInt(numberOfCards-1);
			Card temp = cards[i];
			this.cards[i]= cards[randomNum];
			cards[randomNum]= temp;
				
		}
	}
	
}