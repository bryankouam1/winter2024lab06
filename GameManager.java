public class GameManager{
	private Deck drawPile;
	private Card centerCard;
	private Card playerCard;
	
	public int getDrawPile(){
		return this.drawPile.length();
	}
	public Card getCenterCard(){
		return this.playerCard;
	}
	
	public Card getPlayerCard(){
		return this.playerCard;
	}
	
	public GameManager(){
		Deck newDeck = new Deck();
		this.drawPile = newDeck;
		this.drawPile.shuffle();
		this.centerCard = drawPile.drawTopCard();
		this.playerCard = drawPile.drawTopCard();
	}
	public String toString(){
		return "Center card: "+this.centerCard+"\nPlayer card: "+this.playerCard;
	}
	
	public void dealCards(){
		
		this.drawPile.shuffle();
		this.centerCard=drawPile.drawTopCard();
		this.drawPile.shuffle();
		this.playerCard=drawPile.drawTopCard();
		
	}
	public int getNumberOfCards(){
		return this.drawPile.getNumberOfCards();
	}
	
	public int calculatePoints(){
		if(centerCard.getValue()== playerCard.getValue()){
			return 4;
		}
		else if(centerCard.getSuit() == playerCard.getSuit()){
			return 2;
		}
		else{
			return -1;
		}
	}	
}